/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
const firebaseConfig = {
  apiKey: "AIzaSyDD3ol0M-Ie-on0yAyaK8aAZlUVV2GSrHA",
  authDomain: "gym-buddy-e176e.firebaseapp.com",
  databaseURL: "https://gym-buddy-e176e.firebaseio.com",
  storageBucket: "gym-buddy-e176e.appspot.com",
  messagingSenderId: "1048450686783"
};
const firebaseApp = firebase.initializeApp(firebaseConfig);

import * as firebase from 'firebase';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View, 
  Image,
  TextInput,
  Picker 
} from 'react-native';

export default class client extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      age: '2',
    };
  }

  render() {
    let pic = {
      uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
    };
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to GymBuddy!
        </Text>
        <Text style={styles.instructions}>
          Login:
        </Text>
        <Text style={styles.fields}>
          Bio
        </Text>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          multiline = {true}
          numberOfLines = {4}
          placeholder="Share something about yourself"
          onChangeText={(text) => this.setState({text})}
          value={this.state.text}
        />
    </View>
    );

  }
}

/*
        <Greeting style={styles.test} name='Valeera' />
      <Image source={pic} style={{width: 193, height: 110}}/>
      <Picker style={styles.picker}
        selectedValue={this.state.age}
        onValueChange={(age) => this.setState({age})}>
        var ageRange = [],
        <Picker.Item label="1" value="1" />
        <Picker.Item label="2" value="2" />
      </Picker>
      <Blink text='Yes blinking is so great' />
*/
class UselessTextInput extends Component {
  render() {
    return (
      <TextInput
        {...this.props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
        editable = {true}
        maxLength = {40}
      />
    );
  }
}

class Blink extends Component {
  constructor(props) {
    super(props);
    this.state = {showText: true};

    // Toggle the state every second
    setInterval(() => {
      this.setState({ showText: !this.state.showText });
    }, 1000);
  }

  render() {
    let display = this.state.showText ? this.props.text : ' ';
    return (
      <Text>{display}</Text>
    );
  }
}

class Greeting extends Component {
  render() {
    return (
      <Text style={this.props.style}>Hello {this.props.name}!</Text>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  picker: {
     borderBottomColor: '#333333',
     borderBottomWidth: 1
  },
  title: {
    fontSize: 2,
    margin: 10,
  },
  test: {
    fontSize: 40,
    margin: 10,
  },
  fields: {
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('client', () => client);
